import 'package:flutter_users_demo/modules/home/home_binding.dart';
import 'package:flutter_users_demo/modules/home/home_view.dart';
import 'package:flutter_users_demo/modules/login/login_binding.dart';
import 'package:flutter_users_demo/modules/login/login_view.dart';
import 'package:get/get.dart';
import 'package:flutter_users_demo/routes/app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.login,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoutes.home,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
  ];
}
