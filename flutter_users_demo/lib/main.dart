import 'package:flutter/material.dart';
import 'package:flutter_users_demo/modules/login/login_binding.dart';
import 'package:flutter_users_demo/modules/login/login_view.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_users_demo/routes/app_pages.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mini Market App',
      home: const LoginView(),
      initialBinding: LoginBinding(),
      getPages: AppPages.pages,
    );
  }
}
