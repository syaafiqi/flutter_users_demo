import 'package:realm/realm.dart';

abstract class RealmInstance {
  static final Configuration _configuration = Configuration.local(
    [
      // Add schemas here
    ],
    shouldDeleteIfMigrationNeeded: true,
  );

  static Realm open() => Realm(_configuration);
}
