import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  int id;
  String fullName;
  String birthDate;
  String gender;
  String city;

  UserModel({
    required this.id,
    required this.fullName,
    required this.birthDate,
    required this.gender,
    required this.city,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
