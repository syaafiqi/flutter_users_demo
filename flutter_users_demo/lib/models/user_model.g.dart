// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      id: json['id'] as int,
      fullName: json['fullName'] as String,
      birthDate: json['birthDate'] as String,
      gender: json['gender'] as String,
      city: json['city'] as String,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'fullName': instance.fullName,
      'birthDate': instance.birthDate,
      'gender': instance.gender,
      'city': instance.city,
    };
