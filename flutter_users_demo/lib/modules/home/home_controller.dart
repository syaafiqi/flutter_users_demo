import 'package:flutter_users_demo/models/user_model.dart';
import 'package:get/get.dart';
import 'package:flutter_users_demo/common/storage/storage.dart';

class HomeController extends GetxController {
  bool isLoading = true;
  UserModel? user;

  @override
  void onInit() {
    super.onInit();
    getUser();
  }

  void getUser() {
    user = Storage.getValue('user');
    isLoading = false;
    update();
  }

  void logout() {
    Storage.clearStorage();
    Get.toNamed("/login");
  }
}
