import 'package:flutter/material.dart';
import 'package:flutter_users_demo/modules/home/home_controller.dart';
import 'package:flutter_users_demo/themes/app_text_theme.dart';
import 'package:get/get.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) => Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Container(
              constraints: const BoxConstraints.expand(),
              child: Center(
                child: controller.isLoading
                    ? const CircularProgressIndicator()
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Welcome, ${controller.user?.fullName}",
                            style: robotoRegular(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          ElevatedButton(
                              onPressed: () {
                                controller.logout();
                              },
                              child: Text(
                                "Logout",
                                style: robotoRegular(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              )),
                        ],
                      ),
              )),
        ),
      ),
    );
  }
}
