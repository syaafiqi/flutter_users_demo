import 'package:flutter_users_demo/common/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_users_demo/apis/auth_api.dart';
import 'package:flutter_users_demo/models/user_model.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  AuthApi authApi = AuthApi();
  bool isLoading = false;
  bool isPasswordVisible = false;
  final user = Rxn<UserModel>();

  var username = "";
  var password = "";

  void login() async {
    isLoading = true;
    update();
    var response = await authApi.login(username: username, password: password);
    if (response != null) {
      if (response["success"]) {
        user.value = UserModel.fromJson(response["data"]);
        Storage.saveValue('user', user.value);
      }
    } else {
      Get.snackbar(
        "Login Failed",
        "Invalid credentials.",
        backgroundColor: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
      );
    }
    isLoading = false;
    update();
  }

  void togglePasswordVisibility() {
    isPasswordVisible = !isPasswordVisible;
    update();
  }

  void onNameFieldChanged(String value) {
    username = value;
  }

  void onPasswordFieldChanged(String value) {
    password = value;
  }
}
