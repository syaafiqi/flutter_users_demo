import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_users_demo/common/constants.dart';

class AuthApi {
  final dio = Dio(BaseOptions(
    baseUrl: apiBaseUrl,
    followRedirects: false,
    validateStatus: (status) {
      return status! < 500;
    },
  ));

  Future<dynamic> login({
    required String username,
    required String password,
  }) async {
    final body = {
      "username": username,
      "password": password,
    };
    final response = await dio.post('users/login', data: jsonEncode(body));
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return null;
    }
  }
}
