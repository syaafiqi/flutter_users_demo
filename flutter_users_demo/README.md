# Flutter GetX Boilerplate

A simple yet effective Flutter boilerplate.
Using GetX, Dio, and Realm.

Steps to do:
1. Clone
2. Sync pubspec.yaml
3. Run

Inspired from some source for personal use only!
